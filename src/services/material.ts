import type { Material } from '@/types/Material'
import http from './http'

function addMaterial(material: Material & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', material.name)
  formData.append('quantity', material.quantity.toString())

  if (material.minimum) formData.append('minimum', material.minimum.toString())
  formData.append('unit', material.unit)
  if (material.status) formData.append('status', material.status)
  if (material.files && material.files.length > 0) formData.append('image', material.files[0])
  return http.post('/material', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateMaterial(material: Material & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', material.name)
  formData.append('quantity', material.quantity.toString())

  if (material.minimum) formData.append('minimum', material.minimum.toString())
  formData.append('unit', material.unit)
  if (material.status) formData.append('status', material.status)
  if (material.files && material.files.length > 0) formData.append('image', material.files[0])
  return http.post(`/material/${material.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delMaterial(material: Material) {
  return http.delete(`/material/${material.id}`)
}

function getMaterial(id: number) {
  return http.get(`/material/${id}`)
}

function getMaterials() {
  return http.get('/material')
}

export default {
  addMaterial,
  updateMaterial,
  delMaterial,
  getMaterial,
  getMaterials
}
