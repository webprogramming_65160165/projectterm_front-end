import type { CheckStock } from '@/types/CheckStock'
import http from './http'

function getCheckStocks() {
  return http.get('/check-stock')
}

function addCheckStock(checkstock: CheckStock) {
  return http.post('/check-stock', checkstock)
}

function getLastId() {
  return http.get('/check-stock/last-id')
}
export default {
  getCheckStocks,
  addCheckStock,
  getLastId
}
