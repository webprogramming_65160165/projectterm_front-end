import http from './http'
import CheckMaterialDetail from '@/type/CheckMaterialDetail'

function getCheckDetailStocks() {
  return http.get('/check-material-detail')
}

function addCheckDetailStock(checkMaterialDetail: CheckMaterialDetail) {
  return http.post('/check-material-detail', checkMaterialDetail)
}
export default {
  getCheckDetailStocks,
  addCheckDetailStock
}
