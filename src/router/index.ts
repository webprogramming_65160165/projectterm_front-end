import { createRouter, createWebHistory } from 'vue-router'
function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      // component: () => import('../views/POS/POS.vue'),
      components: {
        default: () => import('../views/POS/POS.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/AboutView.vue'),
      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salary',
      name: 'salary',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/SalaryView.vue'),
      components: {
        default: () => import('../views/Salary/SalaryView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/stock',
      name: 'stock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/StockView.vue'),
      components: {
        default: () => import('../views/StockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import('../views/UsersView.vue'),
      components: {
        default: () => import('../views/UsersView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/Checkinout-view',
      name: 'checkin',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CheckinCheckout/CheckinoutView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/membership-view',
      name: 'membership',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/MembershipView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/Login/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const defaultTitle = 'DCOFFEE';
  document.title = to.meta.title ? `${to.meta.title} | ${defaultTitle}` : defaultTitle;
  next();
});

router.beforeEach((to, from) => {
  if (to.meta.requireAuth && !isLogin()) {
    return {
      path: '/login',
      query: { redirect: to.fullPath }
    }
  }
})

export default router
