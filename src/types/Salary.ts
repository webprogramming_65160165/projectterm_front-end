import type { User } from './User'
type Salary = {
  idSalary: number
  userId: number
  user?: User
  month: string
  salarybaht: number
  status: string
}

export type { Salary }