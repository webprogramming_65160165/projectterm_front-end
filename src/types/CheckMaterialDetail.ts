import type { CheckStock } from './CheckStock'
import type { Stock } from './Stock'

type CheckMaterialDetail = {
  id?: number
  bfCheck: number
  atCheck: number
  Material: Stock
  checkStock: CheckStock
}

export { type CheckMaterialDetail }
