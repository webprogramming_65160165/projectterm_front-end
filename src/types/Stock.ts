type Stock = {
  id?: number
  name: string
  min: string
  max: string
  until: string
  quantity: string
  image?: string
  status: string
}

export type { Stock }
