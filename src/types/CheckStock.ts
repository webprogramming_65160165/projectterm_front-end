import type { User } from './User'

type CheckStock = {
  id?: number
  checkDate: Date
  // checkRawMaterialDetails: CheckRawMaterialDetail[]
  user: User | null
}

export { type CheckStock }
