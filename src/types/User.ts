import type { Role } from './Role'

type Gender = 'male' | 'female' | 'others'
type User = {
  id?: number
  email: string
  password: string
  fullName: string
  image: string
  gender: Gender // male female others
  roles: Role[] //admin user
}

export type { User, Gender }
