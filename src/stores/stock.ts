import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Stock } from '@/types/Stock'

const lastId = ref(0)

export const useStockStore = defineStore('counter', () => {
  const initialStock: Stock = {
    id: -1,
    name: '',
    min: '',
    max: '',
    quantity: '',
    until: '',
    status: '',
    image: 'noimage.jpg',
    files: []
  }

  const stocks = ref(<Stock[]>[])
  const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))

  const editedStocks = ref<Stock[]>([])

  const copy = ref(<Stock[]>[])

  function initialize() {
    stocks.value = [
      {
        id: 1,
        name: 'Chocolate',
        min: '5',
        max: '30',
        quantity: '4',
        until: 'Kg.',
        status: 'Low Stock'
      },
      {
        id: 2,
        name: 'Coffee Bean',
        min: '10',
        max: '40',
        quantity: '35',
        until: 'Kg.',
        status: 'High Stock'
      },
      {
        id: 3,
        name: 'Milk',
        min: '20',
        max: '50',
        quantity: '25',
        until: 'Ml.',
        status: 'Medium Stock'
      },
      {
        id: 4,
        name: 'Green Tea ',
        min: '10',
        max: '40',
        quantity: '30',
        until: 'Kg.',
        status: 'High Stock'
      },
      {
        id: 5,
        name: 'Strawberry',
        min: '20',
        max: '50',
        quantity: '25',
        until: 'Ml.',
        status: 'Medium Stock'
      },
      {
        id: 6,
        name: 'Caramel',
        min: '30',
        max: '50',
        quantity: '25',
        until: 'Ml.',
        status: 'Low Stock'
      }
    ]
    lastId.value = stocks.value.length + 1
    stocks.value.forEach((stock) => {})
  }

  return {
    initialize,
    stocks,

    editedStock,
    lastId,
    initialStock,
    editedStocks,
    copy
  }
})
