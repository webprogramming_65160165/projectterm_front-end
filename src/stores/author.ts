import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import type axios from 'node_modules/axios/index.cjs'
import type { AxiosError } from 'axios'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const userAuthor = defineStore('author', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()

  const login = async function (email: string, password: string) {
    try {
      loadingStore.doLoad()
      const res = await authService.login(email, password)
      console.log(res.data)
      messageStore.showMessage('Login Success')
      router.push('/')
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      loadingStore.finish()
    } catch (error: any) {
      loadingStore.finish()
      console.log(error.message)
      messageStore.showMessage(error.message)
    }}

    const logout = function () {
        localStorage.removeItem('user')
        localStorage.removeItem('access_token')
        router.push('/login')
    }

    function getCurrentUser(): User | null {
        const strUser = localStorage.getItem('user')
        if (strUser === null) return null
        return JSON.parse(strUser)
    }

    function getToken(): string | null {
        const strToken = localStorage.getItem('access_token')
        if (strToken === null) return null
        return JSON.parse(strToken)
    }
    return { getCurrentUser, login, getToken, logout }
})
