import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const initialMember: Member = {
    id: -1,
    name: '',
    tel: ''
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initialMember)))
  const delDialog = ref(false)
  const openDialog = ref(false)
  const tel = ref('')

  const members = ref<Member[]>([
    { id: 1, name: 'มานะ รักชาติ', tel: '0881234567' },
    { id: 2, name: 'มานี มีใจ', tel: '0887654321' }
  ])

  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }
  function clear() {
    currentMember.value = null
    tel.value = ''
    editedMember.value = JSON.parse(JSON.stringify(initialMember))
  }
  return {
    members,
    currentMember,
    searchMember,
    clear,
    openDialog,
    editedMember,
    delDialog
  }
})
